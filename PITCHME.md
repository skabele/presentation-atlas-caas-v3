# atlas-caas-v3 & caas-spinner-ts

How to implement CaaS during coffee break

---

# FooBar project

1. Why `atlas-caas-v3` + `caas-spinner-ts`
2. Main interface `caas-service.ts`
3. Present just spinned FooBar CaaS:
    1. infrastrucrure
    2. main features
    3. helpers
    4. comment on extensibility
    
---

# atlas-caas-v3

1. Real implementations: Nacex & GLS
2. Usage with Typescript or JS
3. (optional) effect of GLS migration to `atlas-caas-v3`
4. State of package and future versions
    
